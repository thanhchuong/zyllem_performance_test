set JMETER_EXTRAS=%JMETER_HOME%\extras
set REPORT_TITLE="Performance testing - Bulk upload"
set REPORT_FILE="report\test-result.jtl"
del %REPORT_FILE%
"%JMETER_HOME%/bin/jmeter" -t BulkUpload.jmx -Jusers=5 -JloopCount=1 -JrampUp=0 -n -l %REPORT_FILE% 
echo "DONE"
./report.bat
